/* CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#ifndef ClassHelperFunctions_cpp
#define ClassHelperFunctions_cpp

/* findChapter -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Returns the Chapter Node at the Specified Depth
 */
ChapterTree::ChapterNode* ChapterTree::findChapter(int ch) {
	int depth = 1;
	return findChapter(chapter, depth, ch);
}
/* findChapter -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Finds the Chapter Node at the Specified Depth
 */
ChapterTree::ChapterNode* ChapterTree::findChapter(ChapterNode* node, int &depth, int ch) {
	if ( (depth == ch) || (node->nextChapter == NULL) ) {
		return node;
	} else {
		return findChapter(node->nextChapter, ++depth, ch);
	}
}

/* findSection -- SectionTree Member Function -- Private
 * :: Description ::
 * 	Returns the Node at the Specified Depth of the Sibling List
 */
SectionTree::SectionNode* SectionTree::findSection(int sec) {
	int depth = 1;
	return findSection(section, depth, sec);
}
/* findSection -- SectionTree Member Function -- Private
 * :: Description ::
 * 	Finds the node at the specified depth of the sibling list.
 */
SectionTree::SectionNode* SectionTree::findSection(SectionNode* node, int &depth, int sec) {
	if ( (depth == sec) || (node->nextSection == NULL) ) {
		return node;
	} else {
		return findSection(node->nextSection, ++depth, sec);
	}
}

/* findSubSection -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Returns the Node at the Specified Depth of the Sibling List
 */
SubSectionTree::SubSectionNode* SubSectionTree::findSubSection(int sec) {
	int depth = 1;
	return findSubSection(subSection, depth, sec);
}

/* findSubSection -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Finds the Node at the specified depth of the sibling list.
 */
SubSectionTree::SubSectionNode* SubSectionTree::findSubSection(SubSectionNode* node, int &depth, int sec) {
	if ( (depth == sec) || (node->nextSubSection == NULL) ) {
		return node;
	} else {
		return findSubSection(node->nextSubSection, ++depth, sec);
	}
}

/* updateLast -- ChapterTree Member Function -- Private 
 * :: Description ::
 * 	If the node was previously the last node in the list.
 * 	Update the 'last' pointer in the sentinel to point to 
 * 	the newly created node.
 */
void ChapterTree::updateLast(ChapterNode* node) {
	if ( isLast(node) ) {
		setLast(node->nextChapter);
	}
}

/* isLast -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Checks if the given node is currently set as the 'last'
 * 	element in the sentinel.
 */ 
bool ChapterTree::isLast( const ChapterNode* node ) const {
	return ( last == node ) ? true : false;
}

/* setLast -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Updates the sentinel's last pointer value.
 */
void ChapterTree::setLast(ChapterNode*& node) {
	last = node;
}

/* isEmpty -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Checks if the list is empty.
 */ 
bool ChapterTree::isEmpty() const {
	return ( chapter == NULL ) ? true : false;
}

/* updateLast -- SectionTree Member Function -- Private 
 * :: Description ::
 * 	If node was previously the last node in the list,
 * 	then update the list's sentinel to point to the newly
 * 	created last node.
 */
void SectionTree::updateLast(SectionNode* node) {
	if ( isLast(node) ) {
		setLast(node->nextSection);
	}
}

/* isLast -- SectionTree Member Function -- Private
 * :: Description ::
 *	Checks if the given node is set as the last element in
 *	the list.
 */ 
bool SectionTree::isLast( const SectionNode* node ) const {
	return ( last == node ) ? true : false;
}

/* setLast -- SectionTree Member Function -- Private
 * :: Description ::
 *	Updates Section's Sentinel last pointer to the given node.
 */
void SectionTree::setLast(SectionNode*& node) {
	last = node;
}

/* isEmpty -- SectionTree Member Function -- Private
 * :: Description ::
 * 	Checks if list is empty.
 */ 
bool SectionTree::isEmpty() const {
	return ( section == NULL ) ? true : false;
}

/* updateLast -- SubSectionTree Member Function -- Private 
 * :: Description ::
 * 	Updates the sentinel's last pointer to point to the correct element.
 */
void SubSectionTree::updateLast(SubSectionNode* node) {
	if ( isLast(node) ) {
		setLast(node->nextSubSection);
	}
}

/* isLast -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Checks if given node is set as the last element in the list.
 */ 
bool SubSectionTree::isLast( const SubSectionNode* node ) const {
	return ( last == node ) ? true : false;
}

/* setLast -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Set the sentinel's last pointer to the given node.
 */
void SubSectionTree::setLast(SubSectionNode*& node) {
	last = node;
}

/* isEmpty -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Checks if list is empty.
 */ 
bool SubSectionTree::isEmpty() const {
	return ( subSection == NULL ) ? true : false;
}


#endif // ClassHelperFunctions_cpp
