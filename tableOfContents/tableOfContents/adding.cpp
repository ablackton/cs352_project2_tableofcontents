/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include "tableOfContents.h"

#ifndef Adding_cpp
#define Adding_cpp

/* addChapter -- BookTree Member Function -- Public
 * :: Description ::
 * 	Used to populate the Chapters for the initial Table of Contents.
 * 	Adds a new chapter to the end of the Chapter's sibling list.
 *
 * :: Example ::
 * 	book->addChapter("NewChapter");
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     \- Section2
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |- ChapterTwo
 * 	  |  |- Section1
 * 	  |  \- Section2
 * 	  \- NewChapter
 */
void BookTree::addChapter( string name ) {
	this->book->toChapters->addChapter( name );
}

/* addChapter -- ChapterTree Member Function -- Public
 * :: Description ::
 * 	Calls the function to modify the chapter sibling list.
 */
void ChapterTree::addChapter( string name ) {
	//addChapter( chapter, name );
	addChapterSkip(name);
}

/* addChapterSkip -- ChapterTree Member Function -- Private
 * :: Details ::
 * 	Adds chapter to the end of the list of chapters.
 */
void ChapterTree::addChapterSkip(string name) {
	if ( isEmpty() ) {
		chapter = new ChapterNode(name);
		setLast(chapter);
	} else {
		last->nextChapter = new ChapterNode(name);
		setLast(last->nextChapter);
	}
}

/* addSection -- BookTree Member Function -- Public
 * :: Description ::
 *	Append section to the last chapter of the book.
 *
 * :: Example ::
 * 	book->addSection("NewSection");
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     \- Section2
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     |- Section2
 * 	     \- NewSection
 */
void BookTree::addSection( string name ) {
	this->book->toChapters->addSection( name );
}

/* addSection -- ChapterTree Member Function -- Public
 * :: Description ::
 *	Adds Section to the Last Chapter in List
 */
void ChapterTree::addSection(string name) {
	last->toSections->addSection(name);
}

/* addSection -- SectionTree Member Function -- Public
 * :: Description ::
 */
void SectionTree::addSection( string name ) { 
	addSectionSkip( name );
}

/* addSectionSkip -- SectionTree Member Function -- Private
 */
void SectionTree::addSectionSkip( string name ) {
	if ( isEmpty() ) {
		section = new SectionNode(name);
		setLast(section);
	} else {
		last->nextSection = new SectionNode(name);
		setLast(last->nextSection);
	}
}

/* addSubSection -- BookTree Member Function -- Public
 * :: Description ::
 * 	Appends a SubSection to the end of the book.
 * 	** Assumption ** 
 * 	There is a Chapter with a Section already in the book.
 * 	The last chapter has a section.
 *
 * :: Example ::
 * 	book->addSubSection("NewSubSection");
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     \- Section2
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     \- Section2
 * 	        \- NewSubSection
 */
void BookTree::addSubSection( string name ) {
	this->book->toChapters->addSubSection( name );
}

/* addSubSection -- ChapterTree Member Function -- Public
 * :: Description ::
 * 	Adds SubSection to the Last Chapter in the Sibling List
 */
void ChapterTree::addSubSection(string name) {
	//addSubSection(chapter, name);
	last->toSections->addSubSection(name);
}

/* addSubSection -- SectionTree Member Function -- Public
 * :: Description ::
 * 	Adds SubSection to the last section in the Sibling List.
 */
void SectionTree::addSubSection( string name ) { 
	//addSubSection( section, name );
	last->toSubSections->addSubSection( name );
}

/* addSubSection -- SubSectionTree Member Function -- Public
 * :: Description ::
 * 	Adds SubSection after the last element in the sibling list.
 */
void SubSectionTree::addSubSection( string name ) {
	//addSubSection( subSection, name );
	addSubSectionSkip(name);
}

/* addSubSectionSkip -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Adds SubSection as the last node in the SubSection Sibling List.
 * 	If the list is empty create first node in list.
 */
void SubSectionTree::addSubSectionSkip( string name ) {
	if (isEmpty()) {
		subSection = new SubSectionNode(name);
		setLast(subSection);
	} else {
		last->nextSubSection = new SubSectionNode(name);
		setLast(last->nextSubSection);
	}
}

/* addParagraph -- BookTree Member Function -- Public
 * :: Description ::
 * 	Adds pages to the last SubSection of the last section of the last chapter.
 *
 * :: Example ::
 * 	book->addParagraph(10);
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     \- Section1
 * 	        \- SubSection1 - 10
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     \- Section1
 * 	        \- SubSection1 - 20
 */
void BookTree::addParagraph( int length ) {
	this->book->toChapters->addParagraph( length );
}

/* addParagraph -- ChapterTree Member Function -- Public
 * :: Description ::
 *   Adds Paragraph to the Last Chapter's Section
 */
void ChapterTree::addParagraph(int length) {
	last->toSections->addParagraph(length);
}

/* addParagraph -- SectionTree Member Function -- Public
 * :: Description ::
 * 	Adds pages to the Last Section's SubSection
 */
void SectionTree::addParagraph( int length ) { 
	last->toSubSections->addParagraph( length );
}

/* addParagraph -- SubSectionTree Member Function -- Public
 * :: Description ::
 * 	Add pages to the last subsection in the sibling list.
 */
void SubSectionTree::addParagraph( int length ) {
	addPages(last, length);
}

#endif // Adding_cpp
