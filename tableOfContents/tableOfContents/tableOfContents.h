/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string>
#include <ctime>

using namespace std;

#define debug
#ifdef debug
//#define addParagraphDebug
#endif //debug

#ifndef TableTree_h
#define TableTree_h

#define Tree_h
#ifndef Tree_h
class Tree {
	private:
		struct Node {
			Node*	sibling;
			Node*	child;

			string 	title;

			Node( const string name ) : 
				title(name), sibling(nullptr), child(nullptr) {}
			Node( const string name, Node* next ) :
				title(name), sibling(next), child(nullptr) {}

		};

		Node*	first;
		Node*	last;

		Node* find(int depth) const;
		Node* find(Node* node, int &depth, int at);
		void add(Node* & node, string name);

		bool isEmpty() const;
		bool isLast(const Node* node);
		void updateLast(Node* node);
		void setLast(Node*& node);

	public:
		Tree();
		virtual print() const;
};
#endif // Tree_h

#ifndef SubSection_h
#define SubSection_h

class SubSectionTree {
	private:
	struct SubSectionNode {
		SubSectionNode* nextSubSection;

		// Sub-Section Comparable Items/Objects
		string	subSectionTitle;
		int	pageCount;

		SubSectionNode( const string title ) : subSectionTitle( title ),
			nextSubSection( nullptr ), pageCount(0) {}

		SubSectionNode( const string title, SubSectionNode* next ) : subSectionTitle( title ),
			nextSubSection( next ), pageCount(0) {}
	};

	// Root of Current Sub-Section Tree
	SubSectionNode*	subSection;
	SubSectionNode*	last;

	void addSubSectionSkip( string name );

	void print(SubSectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, int &subSectionCount) const;
	void print(SubSectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, int &subSectionCount, const int ch, const int sec) const;
	
	void formattedPrint(SubSectionNode * node, const int pageCount, const int chapterCount, const int sectionCount, int &subSectionCount) const;

	void addSubSection(SubSectionNode * & node, string name);
	void addParagraph(SubSectionNode * node, int length);

	void addPages(SubSectionNode*& node, int pc);

	SubSectionNode* findSubSection(int sec); 
	SubSectionNode* findSubSection(SubSectionNode* node, int &depth, int sec);

	bool isEmpty() const;
	bool isLast(const SubSectionNode* node) const;
	void updateLast(SubSectionNode* node);
	void setLast(SubSectionNode*& node);

	public:
	SubSectionTree();
	~SubSectionTree();

	void addSubSection( string name );
	void addParagraph( int length );

	void print(int &pageCount, int &chapterCount, int &sectionCount) const;
	void print(int &pageCount, int &chapterCount, int &sectionCount, const int ch, const int sec) const;

	// Insert Elements into Your Tree
	void insertFirst(string name);
	void insertSubSection( int after, string name);
	void insertParagraph( int ss, int length);

};

#endif // SubSection_h

#ifndef Section_h
#define Section_h

class SectionTree {
	private:
	struct SectionNode {
		SectionNode*	nextSection;
		SubSectionTree*	toSubSections;

		// Section Comparable Items/Objects
		string	sectionTitle;

		SectionNode( const string title ) : sectionTitle( title ), nextSection( nullptr ),
	      		toSubSections( new SubSectionTree ) {}

		SectionNode( const string title, SectionNode* next ) : sectionTitle( title ), 
			nextSection( next ), toSubSections( new SubSectionTree ) {}
	};

	// Root of Current Section Tree
	SectionNode*	section;
	SectionNode*	last;

	void print(SectionNode * node, int &pageCount, int &chapterCount, int &sectionCount) const;
	void print(SectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, const int ch, const int sec) const;

	void formattedPrint(SectionNode* node, const int pageCount, const int chapterCount, int &sectionCount) const;

	void addSectionSkip(string name);

	void addSection(SectionNode * & node, string name);
	void addSubSection(SectionNode * node, string name);
	void addParagraph(SectionNode * node, int length);

	SectionNode* findSection(int sec);
	SectionNode* findSection(SectionNode* node, int &depth, int sec);

	bool isEmpty() const;
	bool isLast(const SectionNode* node) const;
	void updateLast(SectionNode* node);
	void setLast(SectionNode*& node);

	public:
	SectionTree();
	~SectionTree();

	void print(int &pageCount, int &chapterCount) const;
	void print(int &pageCount, int &chapterCount, const int ch, const int sec) const;

	void addSection(string name);
	void addSubSection(string name);
	void addParagraph(int length);

	// Insert Elements into Your Tree
	void insertFirst(string name);
	void insertSection( int after, string name);
	void insertSubSection( int sec, int after, string name);
	void insertParagraph( int sec, int ss, int length);

};
#endif // Section_h

#ifndef Chapter_h
#define Chapter_h

class ChapterTree {
	private:
	struct ChapterNode {
		ChapterNode*	nextChapter;
		SectionTree*	toSections;

		// Chapter Comparable Items/Objects
		string	chapterTitle;

		ChapterNode( string name ) : chapterTitle( name ), nextChapter( nullptr ), 
			toSections( new SectionTree ) { }
		ChapterNode( string name, ChapterNode* next ) : 
			chapterTitle( name ), 
			nextChapter( next ), 
			toSections( new SectionTree ) { }
	};

	// Root of Current Chapter Tree
	ChapterNode* chapter;
	ChapterNode* last;

	bool isEmpty() const;
	bool isLast(const ChapterNode* node) const;
	void updateLast(ChapterNode* node);
	void setLast(ChapterNode*& node);

	void print(ChapterNode * node, int &pageCount, int &chapterCount) const;
	void print(ChapterNode * node, int &pageCount, int &chapterCount, const int ch, const int sec) const;

	void formattedPrint(ChapterNode* node, const int pageCount, int & chapterCount) const;
	
	void addChapterSkip(string name);

	void addChapter(ChapterNode * & node, string name );
	void addSection(ChapterNode * node, string name);
	void addSubSection(ChapterNode * node, string name);
	void addParagraph(ChapterNode * node, int length);

	void insertChapter(ChapterNode * & node, int after, int depth, string name);
	void insertSection(ChapterNode * node, int depth, int ch, int after, string name);

	ChapterNode* findChapter(int ch);
	ChapterNode* findChapter(ChapterNode* node, int &depth, int ch);

	public:
	ChapterTree();
	~ChapterTree();

	void print(int& pageCount) const;
	void print(int& pageCount, const int ch, const int sec) const;

	// Add Elements to the Bottom of the Tree
	void addChapter(string name);
	void addSection(string name);
	void addSubSection(string name);
	void addParagraph(int length);

	// Insert Elements into Your Tree
	void insertFirst(string name);
	void insertChapter( int after, string name );
	void insertSection( int ch, int after, string name );
	void insertSubSection( int ch, int sec, int after, string name );
	void insertParagraph( int ch, int sec, int ss, int length );

};

#endif //Chapter_h

#ifndef Book_h
#define Book_h

class BookTree {
	private:
	struct BookNode {
		BookNode*	nextBook;
		ChapterTree*	toChapters;

		// Book Comparable Item
		string	bookTitle;

		BookNode( const string title ) :
			bookTitle( title ), nextBook( nullptr ), toChapters( new ChapterTree() ) {}
	};

	// Root of Current Book
	BookNode*	book;

	void print(BookNode * node, int & pageCount ) const;
	void print(BookNode * node, int & pageCount, const int ch, const int sec ) const;

	void formattedPrint(BookNode * node) const;

	public:

	BookTree();
	BookTree(string name);
	~BookTree();

	void print() const; // Accessor Function of Book
	void printLookup(const int ch, const int sec) const;

	// Functions to Add Elements to Bottom of Your Tree
	void addChapter( string name );
	void addSection( string name );
	void addSubSection( string name );
	void addParagraph( int length );

	// Insert Elements into Your Tree
	void insertChapter( int after, string name);
	void insertSection( int ch, int after, string name);
	void insertSubSection( int ch, int sec, int after, string name);
	void insertParagraph( int ch, int sec, int ss, int length);


};

#endif // Book_h
#endif //TableTree_h

