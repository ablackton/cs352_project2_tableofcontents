/* CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */
#include "tableOfContents.h"

void parse(string inputFile) {
	const char titleDelim = '`';

	int varEnd, chap, section, subSection, numberOfPages;
	string lineInFile, firstVar, name;

	ifstream fin(inputFile);
	BookTree* book = new BookTree("My Book");

	while (fin) {
		getline(fin, lineInFile);
		varEnd = lineInFile.find(" ");
		firstVar = lineInFile.substr(0, varEnd);

		if (firstVar == "chap") {
			// chap "chapter name" afterCh
			lineInFile = lineInFile.substr(varEnd+2, string::npos);
			varEnd = lineInFile.find(titleDelim);
			name = lineInFile.substr(0,varEnd);
			lineInFile = lineInFile.substr(varEnd+2);
			chap = atoi( &lineInFile.at(0) );

			book->insertChapter(chap, name);

		} else if (firstVar == "sec") {
			// sect "section name" chap afterSec
			lineInFile = lineInFile.substr(varEnd+2, string::npos); // Line reduced to eliminate first argument from the string
			varEnd = lineInFile.find(titleDelim);
			name = lineInFile.substr(0,varEnd);
			lineInFile = lineInFile.substr(varEnd+2); 
			// Line concatenated to third argument of the string
			// Remaining arguments: chapter & section
			chap = atoi( &lineInFile.at(0) );
			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			section = atoi( &lineInFile.at(0) );


			book->insertSection(chap, section, name);

		} else if (firstVar == "sub") {
			// subs "sub section name" chap sec afterSubSec
			lineInFile = lineInFile.substr(varEnd+2, string::npos);
			varEnd = lineInFile.find(titleDelim);
			name = lineInFile.substr(0,varEnd);

			lineInFile = lineInFile.substr(varEnd+2);
			chap = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			section = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			subSection = atoi( &lineInFile.at(0) );

			book->insertSubSection(chap, section, subSection, name);

		} else if (firstVar == "par") {
			// parg numOfPages chap sec subsec afterParg
			lineInFile = lineInFile.substr(varEnd+1, string::npos);
			numberOfPages = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			chap = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			section = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			subSection = atoi( &lineInFile.at(0) );

			book->insertParagraph(chap, section, subSection, numberOfPages);

		} else if (firstVar == "prtb") {
			// prtb
			book->print();
		} else if (firstVar == "prch") {
			// prch ch sec
			lineInFile = lineInFile.substr(varEnd+1, string::npos);
			chap = atoi( &lineInFile.at(0) );

			varEnd = lineInFile.find(" ");
			lineInFile= lineInFile.substr(varEnd+1, string::npos);
			section = atoi( &lineInFile.at(0) );

			book->printLookup(chap, section);
		}
	}

	fin.close();
}
