/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */
#include "tableOfContents.h"

#ifndef PrintingFunctions_
#define PrintingFunctions_

// Printing Column Width Specifiers
// Removes magic numbers from printing functions
const int numberWidthColumn = 50;
const int chapterSectionOffset = 13;
const int subSectionPageCountOffset = 5;


/* print -- BookTree Member Function -- Public
 */
void BookTree::print() const {
	int pageCount = 1;
	print( book, pageCount );

}

/* print -- ChapterTree Member Function -- Public
 */
void ChapterTree::print(int &pageCount) const {
	int chapterCount = 0;
	print( chapter, pageCount, chapterCount );
}

/* print -- SectionTree Member Function -- Public
 */
void SectionTree::print(int &pageCount, int &chapterCount) const {
	int sectionCount = 0;
	print( section, pageCount, chapterCount, sectionCount);
}

/* print -- SubSectionTree Member Function -- Public
 */
void SubSectionTree::print(int &pageCount, int &chapterCount, int &sectionCount) const {
	int subSectionCount = 0;
	print( subSection, pageCount, chapterCount, sectionCount, subSectionCount);
}

/* print -- BookTree Member Function -- Private
 */
void BookTree::print(BookNode * node, int &pageCount ) const {
	if ( node == NULL ) {
		return;
	}
	formattedPrint(node);
	node->toChapters->print(pageCount);
}

/* print -- ChapterTree Member Function -- Private
 */
void ChapterTree::print(ChapterNode * node, int &pageCount, int &chapterCount) const {
	if ( node == NULL ) {
		return;
	}

	formattedPrint(node, pageCount, chapterCount);

	node->toSections->print(pageCount, chapterCount);
	print( node->nextChapter, pageCount, chapterCount );
}

/* print -- SectionTree Member Function -- Private
 */
void SectionTree::print(SectionNode * node, int &pageCount, int &chapterCount, int &sectionCount) const {
	if ( node == NULL ) {
		return;
	}

	formattedPrint(node, pageCount, chapterCount, sectionCount);

	node->toSubSections->print(pageCount, chapterCount, sectionCount);
	print( node->nextSection, pageCount, chapterCount, sectionCount);
}

/* print -- SubSectionTree Member Function -- Private
 */
void SubSectionTree::print(SubSectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, int &subSectionCount) const {
	if ( node == NULL ) {
		return;
	}

	formattedPrint(node, pageCount, chapterCount, sectionCount, subSectionCount);

	pageCount += node->pageCount;
	print( node->nextSubSection, pageCount, chapterCount, sectionCount, subSectionCount);
}


void BookTree::printLookup(int ch, int sec) const {
	int pageCount = 1;
	print( book, pageCount, ch, sec );
}

/* print -- BookTree Member Function -- Private
 */
void BookTree::print(BookNode * node, int &pageCount, int ch, int sec ) const {
	if ( node == NULL ) {
		return;
	}
	formattedPrint(node);
	node->toChapters->print(pageCount, ch, sec);
}

/* print -- ChapterTree Member Function -- Public
 */
void ChapterTree::print(int &pageCount, const int ch, const int sec) const {
	int chapterCount = 0;
	print( chapter, pageCount, chapterCount, ch, sec );
}

/* print -- SectionTree Member Function -- Public
 */
void SectionTree::print(int &pageCount, int &chapterCount, const int ch, const int sec) const {
	int sectionCount = 0;
	print( section, pageCount, chapterCount, sectionCount, ch, sec);
}

/* print -- SubSectionTree Member Function -- Public
 */
void SubSectionTree::print(int &pageCount, int &chapterCount, int &sectionCount, const int ch, const int sec) const {
	int subSectionCount = 0;
	print( subSection, pageCount, chapterCount, sectionCount, subSectionCount, ch, sec);
}

/* print -- ChapterTree Member Function -- Private
 */
void ChapterTree::print(ChapterNode * node, int &pageCount, int &chapterCount, const int ch, const int sec) const {
	if ( node == NULL ) {
		return;
	}

	formattedPrint(node, pageCount, chapterCount);

	node->toSections->print(pageCount, chapterCount, ch, sec);
	print( node->nextChapter, pageCount, chapterCount, ch, sec );
}

/* print -- SectionTree Member Function -- Private
 */
void SectionTree::print(SectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, const int ch, const int sec) const {
	if ( node == NULL ) {
		return;
	}

	if ( chapterCount == ch ) {
		formattedPrint(node, pageCount, chapterCount, sectionCount);
	}

	if ( (chapterCount == ch) && (sectionCount == sec) ) {
		node->toSubSections->print(pageCount, chapterCount, sectionCount);
	} else {
		node->toSubSections->print(pageCount, chapterCount, sectionCount, ch, sec);
	}
	print( node->nextSection, pageCount, chapterCount, sectionCount, ch, sec );
}

/* print -- SubSectionTree Member Function -- Private
 */
void SubSectionTree::print(SubSectionNode * node, int &pageCount, int &chapterCount, int &sectionCount, int &subSectionCount, const int ch, const int sec) const {
	if ( node == NULL ) {
		return;
	}
	pageCount += node->pageCount;
	print( node->nextSubSection, pageCount, chapterCount, sectionCount, subSectionCount, ch, sec);
}


/* Printing Functions Used to Pretty Print 
 * Only called within this file, but if linking errors occur move into the header file.
 */

inline void BookTree::formattedPrint(BookNode * node) const {
	//cout << "Title: " << node->bookTitle << endl;
	string intro = "Table of Contents";
	cout << setw((79+intro.size())/2) << right << intro << endl;
}

inline void ChapterTree::formattedPrint(ChapterNode* node, const int pageCount, int & chapterCount) const {
	cout << "Chapter " << ++chapterCount << ": \t" << setw(numberWidthColumn) << left << node->chapterTitle;// << "\t"; 
	// Right Align Page Count
	cout << setw(chapterSectionOffset) << right << pageCount << endl;
}

inline void SectionTree::formattedPrint(SectionNode * node, const int pageCount, const int chapterCount, int &sectionCount) const {
	cout << "\t" << chapterCount << "." << ++sectionCount << "\t" << setw(numberWidthColumn) << left << node->sectionTitle;
	// Right Align Page Count
	cout << setw(chapterSectionOffset) << right << pageCount << endl;
}

inline void SubSectionTree::formattedPrint(SubSectionNode * node, const int pageCount, const int chapterCount, const int sectionCount, int &subSectionCount) const {
	cout << "\t\t" << chapterCount << "." << sectionCount << "." << ++subSectionCount;
	cout << "\t" << setw(numberWidthColumn) << left << node->subSectionTitle;
	// Right Align Page Count
	cout << setw(subSectionPageCountOffset) << right << pageCount << endl;
}

#endif // PrintingFunctions_
