/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include "tableOfContents.h"

#ifndef InsertFunctions_cpp
#define InsertFunctions_cpp

/* insertChapter -- BookTree Member Function -- Public
 * :: Description ::
 * 	Used to insert a chapter after the node at the specified depth.
 *
 * :: Example ::
 * 	book->insertChapter(2, "NewChapter3");
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |- ChapterTwo
 * 	  |  |- Section1
 * 	  |  \- Section2
 * 	  \- PrevChapter3
 * 	     |- Section1
 * 	     \- Section2
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |- ChapterTwo
 * 	  |  |- Section1
 * 	  |  \- Section2
 * 	  |- NewChapter3
 * 	  \- PrevChapter3
 * 	     |- Section1
 * 	     \- Section2
 */
void BookTree::insertChapter( int after, string name ) {
	if ( after < 1 ) {
		book->toChapters->insertFirst(name);
	} else {
		book->toChapters->insertChapter( after, name );
	}
}

/* insertChapter -- ChapterTree Member Function -- Public
 * :: Description ::
 * 	Inserts a chapter after the specified depth
 */
void ChapterTree::insertChapter( int after, string name ) {
	ChapterNode* afterNode = findChapter(after);
	afterNode->nextChapter = new ChapterNode(name, afterNode->nextChapter);
	updateLast(afterNode);
}

/* insertFirst -- ChapterTree Member Funciton -- Private
 * :: Description ::
 * 	Creates a new Chapter Node at the top of the chapter sibling list.
 */
void ChapterTree::insertFirst(string name) {
	if ( isEmpty() ) {
		chapter = new ChapterNode(name);
		setLast(chapter);
	} else {
		chapter = new ChapterNode(name, chapter);
	}
}

/* insertFirst -- SectionTree Member Funciton -- Public
 * :: Description ::
 * 	Creates a new SectionNode at the top of the section sibling list.
 */
void SectionTree::insertFirst(string name) {
	if ( isEmpty() ) {
		section = new SectionNode(name);
		setLast(section);
	} else {
		section = new SectionNode(name, section);
	}
}

/* insertFirst -- SubSectionTree Member Funciton -- Public
 * :: Description ::
 * 	Creates a new SubSectionNode at the top of the sub-section sibling list.
 */
void SubSectionTree::insertFirst(string name) {
	if ( isEmpty() ) {
		subSection = new SubSectionNode(name);
		setLast(subSection);
	} else {
		subSection = new SubSectionNode(name, subSection);
	}
}

/* insertSection -- BookTree Member Function -- Public
 * :: Description ::
 * 	Used to insert a section after the node at the specified depth.
 *
 * :: Example ::
 * 	book->insertSection(2,1, "NewSection2");
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     \- Section2
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  \- ChapterTwo
 * 	     |- Section1
 * 	     |- NewSection2
 * 	     \- Section2
 */
void BookTree::insertSection(int ch, int after, string name) {
	book->toChapters->insertSection( ch, after, name );
}

/* insertSection -- ChapterTree Member Function -- Public
 * :: Description ::
 * 	Finds the node at the specified depth and calls the insertSection
 * 	function for that chapter's section list.
 */
void ChapterTree::insertSection( int ch, int after, string name ) {
	ChapterNode* node = findChapter(ch);
	if ( after < 1 ) {
		node->toSections->insertFirst(name);
	} else {
		node->toSections->insertSection(after, name);
	}
}

/* insertSection -- SectionTree Member Function -- Public
 * :: Description ::
 *	Inserts a section after the depth of the node specified in the after variable.
 */
void SectionTree::insertSection( int after, string name ) {
	if ( isEmpty() ){
		section = new SectionNode(name);
		setLast(section);
	} else {
		SectionNode* node = findSection(after);
		node->nextSection = new SectionNode(name, node->nextSection);
		updateLast(node);
	}
}

/* insertSubSection -- BookTree Member Function -- Public
 * :: Description ::
 * 	Adds a subsection to the chapter, section, and after depth of subsection specified.
 *
 * :: Example ::
 * 	book->insertSubSection(1,1,1, "NewSubSection");
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |  |- Section1
 * 	  |  |  |- SubSection1
 * 	  |  |  \- SubSection2
 * 	  |  \- Section2
 * 	  \- ChapterTwo
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |  |- Section1
 * 	  |  |  |- SubSection1
 * 	  |  |  |- NewSubSection
 * 	  |  |  \- SubSection2
 * 	  |  \- Section2
 * 	  \- ChapterTwo
 */
void BookTree::insertSubSection(int ch, int sec, int after, string name) {
	book->toChapters->insertSubSection( ch, sec, after, name);
}

/* insertSubSection -- ChapterTree Member Function -- Public 
 * :: Description ::
 * 	Inserts a subsection after the specified chapter, section, and subsection.
 */
void ChapterTree::insertSubSection(int ch, int sec, int after, string name) {
	ChapterNode *node = findChapter(ch);
	node->toSections->insertSubSection(sec, after, name);
}

/* insertSubSection -- SectionTree Member Function -- Public
 * :: Description ::
 * 	Inserts a subsection after the specified section and subsection.
 */
void SectionTree::insertSubSection(int sec, int after, string name) {
	SectionNode* node = findSection(sec);
	if ( after < 1 ) {
		node->toSubSections->insertFirst(name);
	} else {
		node->toSubSections->insertSubSection(after, name);
	}
}

/* insertSubSection -- SubSectionTree Member Function -- Public
 * :: Description ::
 * 	Inserts a subsection after the specified subsection.
 */
void SubSectionTree::insertSubSection(int after, string name) {
	if ( isEmpty() ) {
		subSection = new SubSectionNode(name);
		last = subSection;
	} else {
		SubSectionNode* node = findSubSection(after);
		node->nextSubSection = new SubSectionNode(name, node->nextSubSection);
		updateLast(node);
	}
}

/* insertParagraph -- BookTree Member Function -- Public
 * :: Description ::
 * 	Adds pages to the specified subsection.
 *
 * :: Example ::
 * 	book->insertParagraph(1,1,2,10);
 *
 * 	:: Before ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |  |- Section1
 * 	  |  |  |- SubSection1.1.1 10
 * 	  |  |  |- SubSection1.1.2 10
 * 	  |  |  \- SubSection1.1.3 10
 * 	  |  \- Section2
 * 	  |     \- SubSection1.2.1 0
 * 	  \- ChapterTwo
 * 	:: After ::
 * 	MyBook
 * 	  |- ChapterOne
 * 	  |  |- Section1
 * 	  |  |  |- SubSection1.1.1 10
 * 	  |  |  |- SubSection1.1.2 20
 * 	  |  |  \- SubSection1.1.3 10
 * 	  |  \- Section2
 * 	  |     \- SubSection1.2.1 0
 * 	  \- ChapterTwo
 */
void BookTree::insertParagraph(int ch, int sec, int after, int length) {
	book->toChapters->insertParagraph( ch, sec, after, length);
}

/* insertParagraph -- ChapterTree Member Function -- Public 
 * :: Description ::
 * 	Adds pages to the specified ch, sec, and subsection.
 */
void ChapterTree::insertParagraph(int ch, int sec, int after, int length) {
	ChapterNode *node = findChapter(ch);
	node->toSections->insertParagraph(sec, after, length);
}

/* insertParagraph -- SectionTree Member Function -- Public
 * :: Description ::
 * 	Adds pages to the specified ch, sec, and subsection.
 */
void SectionTree::insertParagraph(int sec, int after, int length) {
	SectionNode* node = findSection(sec);
	node->toSubSections->insertParagraph(after, length);
}

/* insertParagraph -- SubSectionTree Member Function -- Public
 * :: Description ::
 * 	Adds pages to the specified subsection.
 */
void SubSectionTree::insertParagraph(int after, int length) {
	SubSectionNode* node = findSubSection(after);
	addPages(node, length);
}

/* addPages -- SubSectionTree Member Function -- Private
 * :: Description ::
 * 	Add pages to the SubSection's Node
 */
void SubSectionTree::addPages(SubSectionNode*& node, int pc) {
	node->pageCount += pc;
}

#ifdef Call_SuperAnnuated
/* addChapter -- ChapterTree Member Function -- Private
 * :: Description ::
 * 	Antiquated function, no longer called.
 * 	Adds Chapter to the End of the Chapter List by traversing the
 * 	nodes to the end of the list of chapters.
 */
void ChapterTree::addChapter(ChapterNode * & node, string name ) {
	if ( node == NULL ) {
		node = new ChapterNode(name);
	} else {
		addChapter(node->nextChapter, name);
	}
}

#endif // Call_SuperAnnuated


#endif // InsertFunctions_cpp
