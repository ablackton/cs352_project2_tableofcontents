/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include "tableOfContents.h"
#include "classBirthDeath.cpp"
#include "printing.cpp"
#include "parsing.cpp"
#include "insertion.cpp"
#include "adding.cpp"
#include "redundancies.cpp"

void main(int argc, char* argv[]) {
	string filename;
	if ( argc > 1 ) {
		// If the user specified an input file as a command line argument then use that
		filename = argv[1];
	} else {
		// otherwise request the user to specify an input file
		cout << "Please specify an input file: " ;
		getline(cin, filename);
	}
	cout << "Now parsing file: " << filename << endl;
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	// Keep track of how many processor cycles it takes to finish the operations specified in the file
	clock_t time = clock();

	parse(filename);

	time = clock() - time;
	float executionTime = ((float)time)/CLOCKS_PER_SEC;
	cout << "=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=" << endl;
	cout << endl << "Operations Completed (" << executionTime 
		<< " seconds)\n\npress <ENTER> to quit" << endl;
	cin.get();
}
