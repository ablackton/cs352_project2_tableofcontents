/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include "tableOfContents.h"

#ifndef Constructors_cpp
#define Constructors_cpp

/* BookTree Constructor -- Public
 * :: Description ::
 * 	Creates a new book tree structure for the Table of Contents. 
 * 	The BookNode constructor calls the ChapterTree constructor, so a substructure
 * 	is beginning to form.
 * 	The book will have a title as specified in the name field of the constructor.
 *
 * :: Example ::
 * 	BookTree* book = new BookTree("My New Book");
 */
BookTree::BookTree( string name ) {
	book = new BookNode( name );
}

/* ChapterTree Constructor -- Public
 * :: Description :: 
 * 	Sets the initial list of chapters to NULL.
 * 	Called by BookNode constructor to set the toChapters pointer.
 */
ChapterTree::ChapterTree() {
	chapter = NULL;
}

/* SectionTree Constructor -- Public
 * :: Description ::
 * 	Sets the initial list of sections to NULL.
 * 	Called by the ChapterNode constructor to set the toSections pointer.
 */
SectionTree::SectionTree() {
	section = NULL;
}

/* SubSection Constructor -- Public
 * :: Description ::
 * 	Sets the initial list of SubSections to NULL.
 * 	Called by the SectionNode constructor to set the toSubSections pointer.
 */
SubSectionTree::SubSectionTree() {
	subSection = NULL;
}

#endif //Constructors_cpp


#ifndef Destructors_cpp
#define Destructors_cpp

/* BookTree Destructor -- Public
 * :: Description ::
 * 	NOT IMPLEMENTED, not necessary for project specifications.
 * 	No deletions will be performed, so this functionality can be ignored.
 */
BookTree::~BookTree() {
}

/* ChapterTree Destructor -- Public
 * :: Description ::
 * 	NOT IMPLEMENTED, not necessary for project specifications.
 * 	No deletions will be performed, so this functionality can be ignored.
 */
ChapterTree::~ChapterTree() {
}

/* SubSection Destructor -- Public
 * :: Description ::
 * 	NOT IMPLEMENTED, not necessary for project specifications.
 * 	No deletions will be performed, so this functionality can be ignored.
 */
SubSectionTree::~SubSectionTree() {
}

#endif //Destructors_cpp
