/*
 * CS352 Project #2 -- Table of Contents
 * Andrew Blackton <aabxvc@mail.umkc.edu>
 * CS352 Data Structures and Algorithms
 * Professor Appie Van De Liefvoort
 */

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctime>
using namespace std;

#ifndef TableTree_h
#define TableTree_h

// Paragraph's Should just be an ordered linked-list, containing number of pages
class ParagraphList {
	private:

	// Paragraph Node -- Bottom of Tree
	struct ParagraphNode {
		ParagraphNode* nextParagraph;
		int	paragraphLength;

		ParagraphNode( const int & paraLength, ParagraphNode* next) :
			paragraphLength( paraLength ), nextParagraph( next );
	};

	ParagraphNode*	paragraph;

	public:

}

class SubSectionTree {
	private:
	struct SubSectionNode {
		SubSectionNode* nextSubSection;
		ParagraphList* 	toParagraphs;
		//SubSectionInfo* subSectionInformation;

		// Sub-Section Comparable Items/Objects
		String*	subSectionTitle;
		int	subSectionOrder;

		SubSectionNode( const String* ssTitle, const int & order, 
				SubSectionNode* nextSS, ParagraphList* toPara) :
			subSectionTitle( ssTitle ), subSectionOrder( order ), 
			nextSubSection( nextSS ), toParagraphs( toPara );
	};

	// Root of Current Sub-Section Tree
	SubSectionNode*	subSection;

	public:
}

class SectionTree {
	private:
	struct SectionNode {
		SectionNode*	nextSection;
		SubSectionTree*	toSubSections;

		// Section Comparable Items/Objects
		String*	sectionTitle;
		int	sectionOrder;

		SectionTree( const String* title, const int & order, SectionNode* next, 
				SubSectionTree* to) :
			sectionTitle( title ), sectionOrder( order ), nextSection( next ),
			toSubSections( to );
	};

	// Root of Current Section Tree
	SectionNode*	section;

	public:
}

class ChapterTree {
	private:
	struct ChapterNode {
		ChapterNode*	nextChapter;
		SectionTree*	toSections;

		// Chapter Comparable Items/Objects
		String*	chapterTitle;
		int	chapterOrder;

		ChapterNode( const String* name, const int & order, ChapterNode* next,
				SectionTree* to ) :
			chapterTitle( name ), chapterOrder( order ), nextChapter( next ),
			toSections( to );

	};

	// Root of Current Chapter Tree
	ChapterNode* chapter;

	public:
}

class BookTree {
	private:
	struct BookNode {
		BookNode*	nextBook;
		ChapterTree*	toChapters;

		// Book Comparable Item
		String*	bookTitle;

		BookNode( const String* title, BookNode* next, ChapterTree* to ) :
			bookTitle( title ), nextBook( next ), toChapters( to );
	};

	// Root of Current Book
	BookNode*	book;

	public:

	
}

class TableTree {
	private:
	// Paragraph Node -- Bottom of Tree
	struct ParagraphNode {
		ParagraphNode* nextParagraph;
		ParagraphInfo* paragraphInformation;
	};
	
	// Sub-Section Node
	struct SubSectionNode {
		SubSectionNode* nextSubSection;
		ParagraphNode* firstParagraph;
		SubSectionInfo* subSectionInformation;
	};
	
	// Section Node
	struct SectionNode {
		SectionNode* nextSection;
		SubSectionNode* firstSubSection;
		SectionInfo* sectionInformation;
	};
	
	// Chapter Node
	struct ChapterNode {
		ChapterNode* nextChapter;
		SectionNode* firstSection;
		ChapterInfo* chapterInformation;
	};
	
	// Book Node -- Tree Root
	struct BookNode {
		BookInfo* bookInformation;
		ChapterNode* firstChapter;
	};

	public:

}

#endif //TableTree_h
